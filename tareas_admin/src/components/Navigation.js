import React, { Component } from 'react';

import {Tareas} from '../tareas.json';

class Navigation extends Component{
    constructor(){
        super();
        this.state = {
            Tareas
          } 
    }

    render(){
        return (
            <nav className="navbar navbar-dark bg-dark">
            <a href="." className="text-white">
                {this.props.titulo}
                <span className="badge badge-pill badge-light ml-2">
                    {this.state.Tareas.length}

                </span>
            </a>
            </nav>
        );
    }
}

export default Navigation;
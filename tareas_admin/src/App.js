import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Navigation from './components/Navigation';
import {Tareas} from './tareas.json';
//console.log(Tareas);

class App extends Component{

  //El constructor es un método apenas el componente es creado
  //es el primero que se ejecuta del componente 
  //ahí podemos guardar los datos temporalmente que vienen del backen y los muestra en pantalla
  constructor(){
    //super hereda toda la funcionalidad del componente de react
    super();
    //Es el estado de los datos en una aplicación de react
    this.state = {
      Tareas
    } 
  }

  render (){
    //.map función de js para recorrer un arreglo
    //i= indice
    //tarea = parametro 
    const lista_tareas = this.state.Tareas.map((tarea,i) => {
      return(
          <div className="col-md-4">
              <div className="card mt-4">
                  <div className="card-header">
                    <h3>{tarea.titulo}</h3>
                    <span className="badge badge-pill badge-danger ml-2">
                      {tarea.prioridad}
                    </span>
                  </div>
                  <div className="card-body">
                    <p>{tarea.descripcion}</p>
                    <p><mark>{tarea.responsable}</mark></p>
                  </div>

              </div>
          </div>
      )
    })
    return (
      <div className="App">
          <Navigation titulo= "Tareas"/>

           <div className="container">
              <div className="row mt-4">
                {lista_tareas}

              </div>
           </div>
          
          
          
          <img src={logo} className="App-logo" alt="logo" />
               
      </div>
    );
  }
}

export default App;
